use std::collections::{BTreeMap, HashMap};
use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};

#[tokio::main]
async fn main() -> Result<(), Error> {
    let function = handler_fn(calculate);
    lambda_runtime::run(function).await?;
    Ok(())
}

async fn calculate(event: Data, _ctx: Context) -> Result<Freq_Char, Error> {
    let mut freq_char = HashMap::new();
    for c_tmp in event.data.chars() {
        if c_tmp.is_alphabetic() {
            let num = freq_char.entry(c_tmp.to_ascii_lowercase()).or_insert(0);
            *num = *num + 1;
        }
    }
    let sorted_freq: BTreeMap<_, _> = freq_char.into_iter().collect();
    Ok(Freq_Char{freq_char: sorted_freq})
}

#[derive(Serialize, Deserialize)]
struct Data {
    data: String,
}

#[derive(Serialize, Deserialize)]
struct Freq_Char {
    freq_char: BTreeMap<char, usize>
}
