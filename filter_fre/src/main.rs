
use std::collections::{BTreeMap};
use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};

#[tokio::main]
async fn main() -> Result<(), Error> {
    let function = handler_fn(select_low_freq_chars);
    lambda_runtime::run(function).await?;
    Ok(())
}

async fn select_low_freq_chars(event: Char_Freq, _ctx: Context) -> Result<Filter_Char, Error> {
    let mut low_freq = BTreeMap::new();
    for (c, count) in event.result {
        if count <= 5 {
            low_freq.insert(c, count);
        }
    }
    Ok(Filter_Char{low_freq})
}

#[derive(Serialize, Deserialize)]
struct Char_Freq {
    result: BTreeMap<char, usize>,
}

#[derive(Serialize, Deserialize)]
struct Filter_Char {
    low_freq: BTreeMap<char, usize>,
}

