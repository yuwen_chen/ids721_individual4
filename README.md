# ids721_individual4

## Create cargo project and lambda function
Results are shown below:
![image](image_1.png)
![image](image_2.png)

## Deploy the function using the following commands
`cargo lambda build --release`

`cargo lambda deploy`
![image](image_3.png)

## Demonstrations that functions deployed on lambda
![image](image_4.png)
![image](image_5.png)